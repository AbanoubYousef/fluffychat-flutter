# Version 0.8.1 - 2020-02-16
### New features
- Dark mode

# Version 0.8.0 - 2020-02-16
### New features
- Image Viewer
- Improved UX design
- Experimental End-to-end encryption in the web version

# Version 0.7.2 - 2020-02-10
### Changed
- Invite link text
### Fixed
- Replies on replies fixed

# Version 0.7.1 - 2020-02-10
### Fixed
- Replies with correct sender id

# Version 0.7.0 - 2020-02-10
### New features
- Select mode in chat
- Implement replies
- Add scroll down button in chat

# Version 0.6.0 - 2020-02-09
### New features
- Add e2ee settings
- Minor design improvements
### Fixes
- Minor bugs fixed

# Version 0.5.2 - 2020-01-29
### Changes
- New default homeserver: tchncs.de
### Fixes
- Translation fixed
- Parsing of m.room.aliases events

# Version 0.5.1 - 2020-01-28
### Changes
- Refactoring under the hood
### Fixes
- Fixed the bug that when revoking permissions for a user makes the user an admin
- Fixed the Kick user when user has already left the group

# Version 0.5.0 - 2020-01-20
### New features
- Localizations
- Enhanced group settings
- Username search
### Fixes
- Minor bug fixes
- Invite to direct chat

# Version 0.4.0 - 2020-01-18
### New features
- Registration
- Avatars with name letters
- Calc username colors
### Fixes

# Version 0.3.0 - 2020-01-17
### New features
- Improved design
- Implement signing up (Still needs a matrix.org fix)
- Forward messages
- Content viewer
### Fixes
- Chat textfield isn't scrollable on large text
- Text disappears in textfield on resuming the app

# Version 0.2.3 - 2020-01-08
### New features
- Added changelog
- Added copy button
### Fixes
- Groups without name now have a "Group with" prefix
